FROM bitnami/ruby:2.5.5 

#######

LABEL maintainer "BT-Program Systems <programsystems@boystown.org>"

# Install required system packages and dependencies
RUN install_packages ca-certificates ghostscript imagemagick libc6 libgmp-dev libncurses5 libreadline7 libssl1.0.2 libtinfo5 libxml2-dev libxslt1-dev zlib1g zlib1g-dev git libpq-dev libffi-dev nodejs libxrender1 && gem install bundler
RUN sed -i 's/^PASS_MAX_DAYS.*/PASS_MAX_DAYS    90/' /etc/login.defs && \
    sed -i 's/^PASS_MIN_DAYS.*/PASS_MIN_DAYS    0/' /etc/login.defs && \
    sed -i 's/sha512/sha512 minlen=8/' /etc/pam.d/common-password


USER root
RUN [ "sh", "-c", "chmod -R g+rw /opt/bitnami/ruby" ]
RUN [ "sh", "-c", "chgrp -R 0 /opt/bitnami/ruby" ]
USER 1001

ENV BITNAMI_APP_NAME="ruby" \
    BITNAMI_IMAGE_VERSION="2.5.5-debian-9-r120-prod" \
    PATH="/opt/bitnami/ruby/bin:$PATH"

CMD [ "irb" ]
